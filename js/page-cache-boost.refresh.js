/**
 * @file
 * Detects a stale page cache entry and reacts to it.
 */

(function (window, Cookies, Drupal, drupalSettings, console) {

  const cookieName = 'page_cache_boost';
  let timeout = drupalSettings.pageCacheBoost.refreshTimeout;

  function detectUpdatedPage(timeout) {
    if (parseInt(Cookies.get(cookieName))) {
      setTimeout(checkPageUpdates, timeout);
    }
    Cookies.remove(cookieName, { path: document.location.pathname })
  }

  function checkPageUpdates() {
    const request = new XMLHttpRequest();
    request.open('GET', document.location, false);

    request.onload = () => {
      if (request.readyState !== XMLHttpRequest.DONE || request.status !== 200) {
        return;
      }
      if (request.getResponseHeader('X-Drupal-Page-Cache-Boost')) {
        setTimeout(checkPageUpdates, timeout *= 2);
      }
      else {
        const data = {
          url: document.location.href,
          request: request,
        };
        const event = new CustomEvent('page_cache_boost.refresh', { detail:  data });
        window.dispatchEvent(event);
      }
    };

    request.onerror = () => {
      console.error(request.statusText);
    };

    try {
      request.send(null);
    }
    catch (e) {
      console.error(e);
    }
    finally {
      Cookies.remove(cookieName, { path: document.location.pathname })
    }
  }

  Drupal.pageCacheBoost = {

    onRefresh: (data) => {
      try {
        const message = Drupal.t('An updated version of the page is available. <a href="@url">Refresh the page</a>', { '@url': data.url });
        (new Drupal.Message()).add(message, { type: 'warning' });
      }
      catch (e) {
        console.error(e);
      }
    }

  }

  window.addEventListener('page_cache_boost.refresh', (event) => {
    Drupal.pageCacheBoost.onRefresh(event.detail);
  });

  window.addEventListener('load', () => {
    detectUpdatedPage(timeout);
  });

})(window, window.Cookies, Drupal, drupalSettings, console);
