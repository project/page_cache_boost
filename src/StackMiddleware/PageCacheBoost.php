<?php

namespace Drupal\page_cache_boost\StackMiddleware;

use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Site\Settings;
use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

/**
 * Decorates the PageCache middleware logic.
 *
 * This caches pages for anonymous users on a best-effort basis, regenerating
 * page entries after serving stale ones with low a TTL.
 *
 * The middleware service is decorated, but this actually extends the PageCache
 * class, to be able to override/reuse its methods. The only part of the
 * middleware logic that is actually decorated is cached ID generation, which
 * allows this to play nice with the following contrib projects:
 * - advanced_page_cache
 * - page_cache_query_ignore
 */
class PageCacheBoost extends PageCache {

  protected const HEADER_KEY = 'X-Drupal-Page-Cache-Boost';

  /**
   * The application kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $kernel;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The decorated page cache middleware.
   *
   * @var \Drupal\page_cache\StackMiddleware\PageCache
   */
  protected PageCache $pageCache;

  /**
   * Local response cache.
   *
   * @var \Symfony\Component\HttpFoundation\Response[]
   */
  protected array $localCache = [];

  /**
   * An array of page cache regeneration callbacks.
   *
   * @var callable[]
   */
  protected array $regenerationCallbacks = [];

  /**
   * Flag keeping track of whether the regeneration process has been registered.
   *
   * @var bool
   */
  protected bool $regenerationRegistered = FALSE;

  /**
   * PageCacheBoost constructor.
   */
  public function __construct(
    HttpKernelInterface $kernel,
    LockBackendInterface $lock,
    PageCache $page_cache
  ) {
    // When rebuilding the container, the page cache dependencies may be
    // missing.
    if ($page_cache->httpKernel) {
      parent::__construct($page_cache->httpKernel, $page_cache->cache, $page_cache->requestPolicy, $page_cache->responsePolicy);

      $this->kernel = $kernel;
      $this->lock = $lock;
      $this->pageCache = $page_cache;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function lookup(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE) {
    $response = $this->get($request);
    $is_supported_request = $response && $response->headers->get(static::HEADER_KEY) && $type === HttpKernelInterface::MAIN_REQUEST;

    // Skip conditional revalidation if the cache entry is being regenerated.
    if ($is_supported_request) {
      $request->server->remove('HTTP_IF_MODIFIED_SINCE');
      $request->server->remove('HTTP_IF_NONE_MATCH');
    }

    $response = parent::lookup($request, $type, $catch);

    if ($is_supported_request) {
      $response->headers->remove('ETag');
      $response->headers->remove('Last-Modified');

      $this->queueRequestCacheRegenerationJob($request, $type, $catch);
      $this->registerRegeneration();
    }

    return $response;
  }

  /**
   * Queues a request page cache regeneration job.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   * @param int $type
   *   The type of the request (one of HttpKernelInterface::MASTER_REQUEST or
   *   HttpKernelInterface::SUB_REQUEST)
   * @param bool $catch
   *   Whether to catch exceptions or not.
   *
   * @see ::lookup()
   */
  protected function queueRequestCacheRegenerationJob(Request $request, int $type, bool $catch): void {
    $this->regenerationCallbacks[] = function () use ($request, $type, $catch) {
      $this->regenerateCache($request, $type, $catch);
    };
  }

  /**
   * Regenerates a cache entry.
   *
   * This provides cache stampede protection.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   * @param int $type
   *   The type of the request (one of HttpKernelInterface::MASTER_REQUEST or
   *   HttpKernelInterface::SUB_REQUEST)
   * @param bool $catch
   *   Whether to catch exceptions or not.
   *
   * @see ::lookup()
   */
  protected function regenerateCache(Request $request, int $type, bool $catch): bool {
    $lock_name = 'page_cache_boost:' . hash('sha256', static::class . ':' . $this->getCacheId($request));
    $lock_timeout = Settings::get('page_cache_boost.lock_timeout', 30);
    $result = $this->lock->acquire($lock_name, $lock_timeout);
    if ($result) {
      $this->doRegenerateCache($request, $type, $catch);
    }
    return $result;
  }

  /**
   * Actually regenerates a cache entry.
   *
   * This does NOT provide cache stampede protection.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   * @param int $type
   *   The type of the request (one of HttpKernelInterface::MASTER_REQUEST or
   *   HttpKernelInterface::SUB_REQUEST)
   * @param bool $catch
   *   Whether to catch exceptions or not.
   *
   * @see ::lookup()
   */
  protected function doRegenerateCache(Request $request, int $type, bool $catch): void {
    $response = $this->fetch($request, $type, $catch);
    if ($this->kernel instanceof TerminableInterface) {
      $this->kernel->terminate($request, $response);
    }
  }

  /**
   * Registers the regeneration processing task, if needed.
   */
  protected function registerRegeneration(): void {
    if (!$this->regenerationRegistered) {
      $this->registerShutdownFunction(function () {
        foreach ($this->regenerationCallbacks as $regeneration_callback) {
          $regeneration_callback();
        }
      });
      $this->regenerationRegistered = TRUE;
    }
  }

  /**
   * Wrapper method for "drupal_register_shutdown_function".
   *
   * @param callable $function
   *   A shutdown function.
   *
   * @see drupal_register_shutdown_function
   */
  protected function registerShutdownFunction(callable $function): void {
    drupal_register_shutdown_function($function);
  }

  /**
   * {@inheritdoc}
   */
  protected function get(Request $request, $allow_invalid = FALSE) {
    $cid = $this->getCacheId($request);
    $response = &$this->localCache[$cid];

    if (isset($response)) {
      return $response;
    }

    $cache = $this->cache->get($cid, TRUE);
    if (!$cache) {
      $response = FALSE;
      return $response;
    }

    $response = $cache->data;
    if (!$cache->valid && $response instanceof Response) {
      $ttl = Settings::get('page_cache_boost.stale_response_ttl', 5);
      if ($response->getTtl() > $ttl) {
        $response->setMaxAge($ttl);
        $response->setSharedMaxAge($ttl);
      }

      $response->headers->set(static::HEADER_KEY, 1);

      // Since HTTP headers cannot be accessed from Javascript, we set a cookie
      // to indicate a stale page.
      if ($response instanceof HtmlResponse && Settings::get('page_cache_boost.js_enabled', TRUE)) {
        $cookie = Cookie::create('page_cache_boost', 1, 0, $request->getRequestUri(), NULL, TRUE, FALSE, TRUE, Cookie::SAMESITE_STRICT);
        $response->headers->setCookie($cookie);
      }
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCacheId(Request $request) {
    return $this->pageCache->getCacheId($request);
  }

}
