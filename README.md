# Page Cache Boost

This module caches pages for anonymous users on a best-effort basis,
regenerating page entries after serving stale ones with low a TTL.

No configuration is required, just enable the module. You can tweak the stale
response TTL via PHP settings:

```php
$settings['page_cache_boost.stale_response_ttl'] = 10;
```

The module provides cache stampede protection via locks, when rebuilding a page
cache entry. The lock timeout is currently set to 30 seconds, it can be tweaked
via PHP settings:

```php
$settings['page_cache_boost.lock_timeout'] = 60;
```
