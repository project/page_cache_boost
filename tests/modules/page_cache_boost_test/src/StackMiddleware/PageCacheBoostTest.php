<?php

namespace Drupal\page_cache_boost_test\StackMiddleware;

use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;
use Drupal\page_cache\StackMiddleware\PageCache;
use Drupal\page_cache_boost\StackMiddleware\PageCacheBoost;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Test page cache boot extension.
 */
class PageCacheBoostTest extends PageCacheBoost {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * PageCacheBoostTest constructor.
   */
  public function __construct(
    HttpKernelInterface $kernel,
    LockBackendInterface $lock,
    StateInterface $state,
    PageCache $page_cache
  ) {
    parent::__construct($kernel, $lock, $page_cache);

    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  protected function regenerateCache(Request $request, int $type, bool $catch): bool {
    $result = parent::regenerateCache($request, $type, $catch);

    // We only need to track entity test view requests.
    if (!preg_match('%/entity_test/(\d+)%', $request->getPathInfo(), $matches)) {
      return $result;
    }

    // Now track how many times regeneration was invoked and how many times it
    // was actually performed.
    $state_key = 'page_cache_boost_test.entity_view:' . $matches[1];
    while (!$this->lock->acquire($state_key)) {
      $this->lock->wait($state_key);
    }

    $count = (int) $this->state->get($state_key);
    $this->state->set($state_key, $count + 1);

    if ($result) {
      $state_regen_key = $state_key . ':regeneration_count';
      $count = (int) $this->state->get($state_regen_key);
      $this->state->set($state_regen_key, $count + 1);
    }

    $this->lock->release($state_key);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function doRegenerateCache(Request $request, int $type, bool $catch): void {
    // If needed, simulate a long regeneration time.
    $time = (int) $this->state->get('page_cache_boost_test.sleep_time');
    if ($time) {
      sleep($time);
    }

    parent::doRegenerateCache($request, $type, $catch);
  }

}
