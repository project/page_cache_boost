<?php

namespace Drupal\Tests\page_cache_boost\Functional;

use Drupal\Core\State\StateInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Enables the page cache and tests it with various HTTP requests.
 *
 * @group page_cache_boost
 *
 * @coversDefaultClass \Drupal\page_cache_boost\StackMiddleware\PageCacheBoost
 */
class PageCacheBoostTest extends BrowserTestBase {

  use AssertPageCacheContextsAndTagsTrait;

  protected const MISS = 0;
  protected const HIT = 1;
  protected const STALE = 0;
  protected const VALID = 1;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'page_cache_boost',
    'page_cache_boost_test',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->state = $this->container->get('state');

    $config = $this->config('system.performance');
    $config->set('cache.page.max_age', 300);
    $config->save();

    /** @var \Drupal\user\RoleInterface $anonymous_role */
    $anonymous_role = Role::load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('view test entity');
    $anonymous_role->save();
  }

  /**
   * @covers ::get
   * @covers ::getCacheId
   * @covers ::lookup
   * @covers ::queueRequestCacheRegenerationJob
   * @covers ::registerRegeneration
   * @covers ::registerShutdownFunction
   *
   * @throws \Exception
   */
  public function testEntityTestPage(): void {
    $name_1 = $this->randomString();
    $entity_test = EntityTest::create(['name' => $name_1]);
    $entity_test->save();

    // Request the entity test page and check that the first time we have a
    // cache MISS and the subsequent time a HIT, as per the regular core
    // behavior.
    $this->assertCachedRequest($entity_test, $name_1, self::VALID, self::MISS);
    $this->assertCachedRequest($entity_test, $name_1, self::VALID, self::HIT);

    // Update the test entity to invalidate the page cache entry.
    $name_2 = $name_1 . '_2';
    $entity_test->setName($name_2);
    $entity_test->save();

    // Request the entity test page again. This time we expect to get a stale
    // page cache entry with a low TTL.
    $this->assertCachedRequest($entity_test, $name_1, self::STALE, self::HIT);

    // Check that any subsequent request gets a valid page cache entry.
    $this->waitForRegeneration($entity_test, 1);
    $this->assertRegenerationCount($entity_test, 1);
    $this->assertCachedRequest($entity_test, $name_2, self::VALID, self::HIT);

    // Update the test entity to invalidate the page cache entry.
    $name_3 = $name_1 . '_3';
    $entity_test->setName($name_3);
    $entity_test->save();

    // Simulate a high regeneration time.
    $this->state->set('page_cache_boost_test.sleep_time', 5);
    $this->assertCachedRequest($entity_test, $name_2, self::STALE, self::HIT);

    // @todo Apparently DrupalCI processes requests differently than DDEV local
    //   environments: request headers/output is only returned after all
    //   shutdown functions are processed, so we cannot actually simulate
    //   "concurrent" requests.
    $this->waitForRegeneration($entity_test, 2);
/*
    // Check that a request arriving while the cache entry is being regenerated
    // does not trigger another regeneration job, but still gets a fast stale
    // response.
    $this->assertCachedRequest($entity_test, $name_2, self::STALE, self::HIT);

    // So far we had three regeneration attempts, but only two are expected to
    // have succeeded, since two of them were concurrent.
    $this->waitForRegeneration($entity_test, 3);
*/
    $this->assertRegenerationCount($entity_test, 2);

    // Finally check that the entry was correctly regenerated.
    $this->assertCachedRequest($entity_test, $name_3, self::VALID, self::HIT);
  }

  /**
   * Asserts that a request to the entity test view page behaves as expected.
   *
   * @param \Drupal\entity_test\Entity\EntityTest $entity_test
   *   The target entity test entity.
   * @param string $content
   *   The expected page content.
   * @param int $valid
   *   Whether the response should be valid or stale.
   * @param int $hit
   *   Whether page cache should be HIT ot MISS.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function assertCachedRequest(EntityTest $entity_test, string $content, int $valid, int $hit): void {
    $this->drupalGet($entity_test->toUrl());
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->responseHeaderEquals('Cache-Control', $valid ? 'max-age=300, public' : 'max-age=5, public, s-maxage=5');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', $hit ? 'HIT' : 'MISS');
    $this->assertSession()->pageTextContains($content);
  }

  /**
   * Waits until the specified regeneration job is complete.
   *
   * @param \Drupal\entity_test\Entity\EntityTest $entity_test
   *   The target entity test entity.
   * @param int $index
   *   The regeneration identification number.
   */
  protected function waitForRegeneration(EntityTest $entity_test, int $index): void {
    $max_sleep = 0;
    $state_key = 'page_cache_boost_test.entity_view:' . $entity_test->id();
    while (($value = (int) $this->state->get($state_key)) < $index) {
      sleep(1);
      $this->state->resetCache();
      if (++$max_sleep === 10) {
        $this->fail(sprintf('Regeneration #%d failed: %d', $index, $value));
      }
    }
  }

  /**
   * Asserts that the regeneration count matches the expected one.
   *
   * @param \Drupal\entity_test\Entity\EntityTest $entity_test
   *   The target entity test entity.
   * @param int $expected_count
   *   The expected regeneration count.
   */
  protected function assertRegenerationCount(EntityTest $entity_test, int $expected_count): void {
    $regeneration_count = (int) $this->state->get('page_cache_boost_test.entity_view:' . $entity_test->id() . ':regeneration_count');
    $this->assertSame($expected_count, $regeneration_count);
  }

}
